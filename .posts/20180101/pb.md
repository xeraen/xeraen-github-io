# Personal Bests

I speedrun several games, but I'm currently focusing on the Resident Evil series.  Below are all of my Personal Best records.

***Biohazard / Resident Evil***

| Time | Platform | Region | Category | Date |
| ----- | ----- | ----- | ----- | ----- |
| [46m 41s](https://www.instagram.com/p/BQCT_-JDMoD/) | PC | JPN / NTSC | Jill - Bad Ending (Any%) - No Major Glitches | Feb. 2, 2017 |

***Biohazard 2 / Resident Evil 2***

| Time | Platform | Region | Category | Date |
| ----- | ----- | ----- | ----- | ----- |
| [53m 47s](https://www.speedrun.com/re2/run/pyd7r4xy) | PC | JPN / NTSC | Leon A - Any% - Normal | Jan.10, 2016 |
| [1h 24m 56s](https://www.youtube.com/watch?v=--H6ajm-qPA) | PSN/PS3 | USA / NTSC | Leon A - Any% | Dec. 7, 2015 |

***Biohazard 7 / Resident Evil 7***

| Time | Platform | Region | Category | Date |
| ----- | ----- | ----- | ----- | ----- |
| [1h 39m 27s](https://www.youtube.com/watch?v=rke7_AQuz4Q) | PC | N/A | Easy - New Game+ | Feb. 20, 2017 |
