# Welcome!

Hi! I'm Jordan, and I'm involved in several things of interest which include music composition and performance, livestreaming, software development, and sometimes video game speedrunning.  Take a gander at the links above, and feel free to reach out to me on [Twitter](https://twitter.com/jordanbsanders)!
