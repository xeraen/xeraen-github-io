require 'redcarpet'
require 'fileutils'

renderer = Redcarpet::Render::HTML.new(render_options = {})
markdown = Redcarpet::Markdown.new(renderer, extensions = { :tables => true })
header = File.open('layout/header.html').read
footer = File.open('layout/footer.html').read
post_content = ''

Dir.glob('posts/*.md').each do |filepath|
  file = File.open(filepath, 'rb')
  post_name = File.basename(file, File.extname(file))
  post_content = markdown.render(file.read)
  post = header + post_content + footer
  if post_name == 'index'
    File.write("index.html", post)
  else
    FileUtils.mkdir_p(post_name) unless File.directory?(post_name)
    File.write("#{post_name}/index.html", post)
  end
end
